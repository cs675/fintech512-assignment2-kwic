import java.util.*;

public class Input {
    private List<String> words_list;
    private List<String> title_list;
    private ArrayList<ArrayList<String>> word_of_title;
    private TreeMap<String,ArrayList<ArrayList<Integer>>> treemap;
    public Input (String input) {
        this.words_list=new ArrayList<String>();
        this.title_list=new ArrayList<String>();
        this.word_of_title=new ArrayList<ArrayList<String>>();
        String[] in = input.split("\n");
        int flag = 0;
        for (String a : in) {
            if (a.equals("::")) {
                flag = 1;
                continue;
            }
            if (flag == 0) {
                this.words_list.add(a);
            } else if (flag == 1) {
                this.title_list.add(a);
                ArrayList<String> words = new ArrayList<String>();
                String[] w_list = a.split(" ");
                for (String w : w_list) {
                    words.add(w);
                }
                this.word_of_title.add(words);
            }
        }
    }
    public String try_output(){
        String output="";
        for(String w : words_list){
            output+=w+"\n";
        }
        output+="::\n";
        for(String w: title_list){
            output+=w+"\n";
        }
        return output;
    }

    public void create_dictionary(){
        this.treemap=new TreeMap<String,ArrayList<ArrayList<Integer>>> ();
        for(int i=0; i<word_of_title.size(); i++){
            for(int j=0;j<word_of_title.get(i).size();j++){
                String word=word_of_title.get(i).get(j).toLowerCase();
                if(words_list.contains(word)){
                    continue;
                }
                else {
                    ArrayList<ArrayList<Integer>> value;
                    if(treemap.containsKey(word)){
                        ArrayList<Integer> pa = new ArrayList<Integer>();
                        pa.add(i);
                        pa.add(j);
                        value = treemap.get(word);
                        value.add(pa);
                        treemap.put(word, value);
                    }
                    else {
                        value = new ArrayList<ArrayList<Integer>>();
                        ArrayList<Integer> pa = new ArrayList<Integer>();
                        pa.add(i);
                        pa.add(j);
                        value.add(pa);
                        treemap.put(word, value);
                    }
                }
            }
        }
    }

    public String output_dictionary(){
        /*Collection res = treemap.entrySet();
        Iterator i = res.iterator();
        while (i.hasNext()) {
            System.out.println(i.next());
        }*/
        String result="";
        for(String k : treemap.keySet()){
            ArrayList<ArrayList<Integer>> value = treemap.get(k);
            for(ArrayList<Integer> va: value){
                ArrayList<String> matched_line=word_of_title.get(va.get(0));
                for(int i=0;i<matched_line.size();i++){
                    if(i==va.get(1)){
                        result += matched_line.get(i).toUpperCase();
                    }
                    else {
                        result += matched_line.get(i).toLowerCase();
                    }
                    if(i!=matched_line.size()-1){
                        result+=" ";
                    }
                }
                result+="\n";
            }
        }
        result=result.substring(0,result.length()-1);
        return result;
    }

}


